Attribute VB_Name = "ModContabilidad"
Option Explicit

Public Function ADec(ByVal nro As Double) As String
    ADec = Replace(CStr(nro), ",", ".")
End Function

Public Function AFecha(ByVal fecha As String) As String
    If fecha <> "" Then
        AFecha = Format(fecha, "dd/mm/yyyy")
    End If
End Function

Public Function GetDatabase(ByVal server As String, ByVal database As String) As ADODB.Connection
    On Error GoTo Handler
    Dim dbase As ADODB.Connection
    Set dbase = New ADODB.Connection
    With dbase
        .CursorLocation = adUseClient
        .CommandTimeout = 30
        .ConnectionTimeout = 5
        .Provider = "SQLOLEDB.1"
        .Properties("Data Source").Value = server
        .Properties("Initial Catalog").Value = database
        .Properties("Persist Security Info").Value = False
        .Properties("Integrated Security").Value = "SSPI"
        .Open
    End With
    Set GetDatabase = dbase
    Exit Function
Handler:
    MsgBox "Database error " & Err.Number & " : " & Err.Description
End Function

Public Function GetSelect(ByVal query_select As String) As ADODB.Recordset
    On Error GoTo Handler
    Dim dbs As ADODB.Connection
    Set dbs = GetDatabase(GlServidor, GlBaseDatos)
    If Not dbs Is Nothing Then
        Dim result As ADODB.Recordset
        Set result = dbs.Execute(query_select)
        Debug.Print query_select
        Set GetSelect = result
    End If
    Exit Function
CleanExit:
    If Not result Is Nothing Then result.Close
    If Not dbs Is Nothing And dbs.State = adStateOpen Then
        dbs.Close
    End If
    Exit Function
Handler:
    MsgBox ("Select statement error " & Err.Number & " : " & Err.Description)
    Resume CleanExit
End Function

Public Sub ExecProcedure(ByVal query_stored As String)
    On Error GoTo Handler
    Dim dbs As ADODB.Connection
    Set dbs = GetDatabase("192.168.3.131", "CONDOBO")
    
    If Not dbs Is Nothing Then
        Dim stored As ADODB.Command
        Set stored = New ADODB.Command
        With stored
            .ActiveConnection = dbs
            .CommandText = query_stored
            .CommandType = adCmdText
            .Execute
        End With
    End If
CleanExit:
    If Not dbs Is Nothing And dbs.State = adStateOpen Then
        dbs.Close
    End If
    Exit Sub
Handler:
    MsgBox ("Stored Procedure error " & Err.Number & " : " & Err.Description)
    Resume CleanExit
End Sub

Public Sub Contabiliza_Contratos(ByVal venta_codigo As Long)
    On Error GoTo Handler
    'Contabilizacion al momento de aprobacion
    'Vista relativa a contabilizacion
    Dim rs_data99 As New ADODB.Recordset
    'Declaracion de variables
    Dim VAR_CODTIPO As String
    Dim VAR_PARTIDA As String
    Dim VAR_EMPRESA As Integer
    Dim VAR_DPTO As Integer
    Dim VAR_TIPOCOMPID As Integer
    Dim VAR_FECHA As Date
    Dim VAR_MONEDAID As Integer
    Dim VAR_TIPOCAMBIO As Double
    Dim VAR_DEBEORG As Double
    Dim VAR_HABERORG As Double
    'Glosas superiores
    Dim VAR_EntregadoA As String
    Dim VAR_CONCEPTO As String
    'Impuestos
    Dim VAR_PorIVA As Double
    Dim VAR_PorIT As Double
    Dim VAR_PorITF As Double
    'Otros valores
    Dim VAR_ConFac As Integer
    Dim VAR_SinFac As Integer
    Dim VAR_Automatico As Integer
    Dim VAR_GLOSA As String
    Dim VAR_TipoNotaId As Integer
    Dim VAR_NotaNro As Integer
    Dim VAR_EstadoId As Integer
    Dim VAR_iConcurrency_id As Integer
    Dim VAR_TipoAsientoId As Integer
    Dim VAR_CentroCostoId As Integer
    Dim VAR_TipoRetencionId As Integer
    Dim VAR_TipoId As Integer
    Dim VAR_CompDetIdOrg As Integer
    Dim VAR_AuxAna As String
    ' Reverse identification
    Dim cod1 As Long
    Dim idCAutom As Integer
    ' Variables adicionales
    Dim query_data As String
    Dim query_stored As String
    ' Data
    query_data = "SELECT [par], [vtipo], [dpto], [fecha], [tm], [tc], [bs], [dol], CONCAT('RESPONSABLE: ', [beneficiario_codigo], ' - ', [beneficiario_denominacion]) AS EntregadoA, CONCAT('REG. DEVENGADO ', [trans_descripcion2], ' ', [depto_descripcion], ' EDIFICIO ', [edif_codigo_corto], ' ', [edif_descripcion], ' VIGENCIA DEL ', FORMAT([venta_fecha_inicio], 'dd/MM/yyyy'), ' AL ', FORMAT([venta_fecha_fin], 'dd/MM/yyyy'), ' S/G ', [contratoOds], ' ', [unidad_codigo_ant]) AS PorConcepto, [solicitud_tipo], [nro], CONCAT('INGRESO POR: ', [venta_descripcion], '-  NRO. VENTA: ', [nro]) AS glosa, [CentroCostoId], [edif_codigo] FROM [dbo].[conta_contratos] WHERE [cod1] = " & venta_codigo
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect(query_data)
    If rs_data99.State = adStateClosed Then Exit Sub
    If rs_data99.RecordCount > 0 Then
        VAR_CODTIPO = "DEI"
        VAR_PARTIDA = rs_data99!par
        VAR_EMPRESA = IIf(rs_data99!vtipo = "G", 2, 1)
        VAR_DPTO = rs_data99!dpto
        VAR_TIPOCOMPID = 3
        VAR_FECHA = CDate(rs_data99!fecha)
        'VAR_TIPOCAMBIO = IIf(IsNull(rs_data99!tc), GlTipoCambioOficial, rs_data99!tc)
        VAR_TIPOCAMBIO = GlTipoCambioOficial
        VAR_MONEDAID = 1
        VAR_DEBEORG = Round(rs_data99!bs, 2)
        VAR_HABERORG = Round(rs_data99!bs, 2)
'        If rs_data99!tm = "BOB" Then
'            VAR_MONEDAID = 1
'            VAR_DEBEORG = rs_data99!bs
'            VAR_HABERORG = rs_data99!bs
'        Else
'            VAR_MONEDAID = 2
'            VAR_DEBEORG = rs_data99!dol
'            VAR_HABERORG = rs_data99!dol
'        End If
        'Glosas superiores
        VAR_EntregadoA = rs_data99!EntregadoA
        VAR_CONCEPTO = rs_data99!PorConcepto
        'Otros
        ' Impuestos
        VAR_PorIVA = 0.13
        VAR_PorIT = 0.03
        VAR_PorITF = 0.0015
        ' Otros valores
        VAR_ConFac = 0
        VAR_SinFac = 1
        VAR_Automatico = 1 '0 Permite edicion, 1 no permite editar
        VAR_TipoNotaId = rs_data99!solicitud_tipo
        VAR_NotaNro = rs_data99!nro
        ' Glosa general
        VAR_GLOSA = rs_data99!glosa
        VAR_EstadoId = 11 'Libro Mayor requiere que sean de EstadoId = 10 Cerrado OR EstadoId = 11 Abierto
        VAR_TipoAsientoId = 0 ' Operativo
        VAR_CentroCostoId = rs_data99!CentroCostoId
        VAR_TipoRetencionId = 0
        VAR_TipoId = 0
        VAR_CompDetIdOrg = 0
        VAR_AuxAna = rs_data99!edif_codigo
        'Reverse identification
        cod1 = venta_codigo
        idCAutom = 1 'Caso contratos
        ' query_stored
        query_stored = "EXECUTE [dbo].[conta_ingresos] '" & VAR_CODTIPO & "', '" & VAR_PARTIDA & "', " & VAR_EMPRESA & ", " & VAR_DPTO & ", " & VAR_TIPOCOMPID & ", '" & VAR_FECHA & "', " & VAR_MONEDAID & ", '" & ADec(VAR_TIPOCAMBIO) & "', '" & ADec(VAR_DEBEORG) & "', '" & ADec(VAR_HABERORG) & "', '" & VAR_EntregadoA & "', '" & VAR_CONCEPTO & "', '" & ADec(VAR_PorIVA) & "', '" & ADec(VAR_PorIT) & "', '" & ADec(VAR_PorITF) & "', " & VAR_ConFac & ", " & VAR_SinFac & ", " & VAR_Automatico & ", '" & VAR_GLOSA & "', " & VAR_TipoNotaId & ", " & VAR_NotaNro & ", " & VAR_EstadoId & ", '" & glusuario & "', " & VAR_TipoAsientoId & ", " & VAR_CentroCostoId & ", " & VAR_TipoRetencionId & ", " & VAR_TipoId & ", " & VAR_CompDetIdOrg & ", '" & VAR_AuxAna & "', " & venta_codigo & ", " & 0 & ", " & idCAutom
        Debug.Print query_stored
        ' EXEC stored procedured
        Call ExecProcedure(query_stored)
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
Handler:
    If Err.Number <> 0 Then
        MsgBox ("Contratos " & Err.Number & " : " & Err.Description)
    End If
End Sub

Public Sub Contabiliza_Facturacion(ByVal venta_codigo As Long, ByVal cobranza_codigo As Long)
    On Error GoTo Handler
    'Contabilizacion al momento de aprobacion
    'Vista relativa a contabilizacion
    Dim rs_data99 As New ADODB.Recordset
    'Declaracion de variables
    Dim VAR_CODTIPO As String
    Dim VAR_PARTIDA As String
    Dim VAR_EMPRESA As Integer
    Dim VAR_DEPTO As Integer
    Dim VAR_TIPOCOMPID As Integer
    Dim VAR_FECHA As Date
    Dim VAR_MONEDAID As Integer
    Dim VAR_TIPOCAMBIO As Double
    Dim VAR_DEBEORG As Double
    Dim VAR_HABERORG As Double
    'Glosas superiores
    Dim VAR_EntregadoA As String
    Dim VAR_CONCEPTO As String
    'Impuestos
    Dim VAR_PorIVA As Double
    Dim VAR_PorIT As Double
    Dim VAR_PorITF As Double
    'Otros valores
    Dim VAR_ConFac As Integer
    Dim VAR_SinFac As Integer
    Dim VAR_Automatico As Integer
    Dim VAR_GLOSA As String
    Dim VAR_TipoNotaId As Integer
    Dim VAR_NotaNro As Integer
    Dim VAR_EstadoId As Integer
    Dim VAR_iConcurrency_id As Integer
    Dim VAR_TipoAsientoId As Integer
    Dim VAR_CentroCostoId As Integer
    Dim VAR_TipoRetencionId As Integer
    Dim VAR_TipoId As Integer
    Dim VAR_CompDetIdOrg As Integer
    Dim VAR_AuxAna As String
    ' Variables adicionales
    Dim query_data As String
    Dim query_stored As String
    Dim query_stored2 As String
    Dim query_stored3 As String
    ' Data
    query_data = "SELECT [par], [vtipo], [dpto], [fecha], [tm], [tc], [bs], [dol], [nit], [denom], [nrofac], [edif_codigo_corto], [edif_descripcion], [trans_descripcion2], [glosa], [contratoOds], [unidad_codigo_ant], [tiponota], [CentroCostoId], [edif_codigo] FROM [dbo].[conta_facturacion] WHERE [cod1] = " & venta_codigo & " AND [cod2] = " & cobranza_codigo
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect(query_data)
    If rs_data99.State = adStateClosed Then Exit Sub
    If rs_data99.RecordCount > 0 Then
        VAR_CODTIPO = "REF"
        VAR_PARTIDA = rs_data99!par
        VAR_EMPRESA = IIf(rs_data99!vtipo = "G", 2, 1)
        VAR_DEPTO = rs_data99!dpto
        VAR_TIPOCOMPID = 3
        VAR_FECHA = CDate(rs_data99!fecha)
        VAR_TIPOCAMBIO = IIf(IsNull(rs_data99!tc), GlTipoCambioOficial, rs_data99!tc)
        VAR_MONEDAID = 1
        VAR_DEBEORG = Round(rs_data99!bs, 2)
        VAR_HABERORG = Round(rs_data99!bs, 2)
'        If rs_data99!tm = "BOB" Then
'            VAR_MONEDAID = 1
'            VAR_DEBEORG = rs_data99!bs
'            VAR_HABERORG = rs_data99!bs
'        Else
'            VAR_MONEDAID = 2
'            VAR_DEBEORG = rs_data99!dol
'            VAR_HABERORG = rs_data99!dol
'        End If
        'Glosas superiores
        VAR_EntregadoA = "FACTURA A NOMBRE: " & rs_data99!nit & " - " & rs_data99!denom
        VAR_CONCEPTO = "CONTABILIZACION FACTURA NRO. " & rs_data99!nrofac
        VAR_CONCEPTO = VAR_CONCEPTO & " - EDIFICIO: " & rs_data99!edif_codigo_corto & " " & rs_data99!edif_descripcion
        VAR_CONCEPTO = VAR_CONCEPTO & " " & rs_data99!trans_descripcion2 & " " & rs_data99!glosa
        VAR_CONCEPTO = VAR_CONCEPTO & " SEGUN " & rs_data99!contratoOds & " " & rs_data99!unidad_codigo_ant
        'Otros
        ' Impuestos
        VAR_PorIVA = 0.13
        VAR_PorIT = 0.03
        VAR_PorITF = 0.0015
        ' Otros valores
        VAR_ConFac = 0
        VAR_SinFac = 1
        VAR_Automatico = 1 '0 Permite edicion, 1 no permite editar
        VAR_TipoNotaId = rs_data99!tiponota
        VAR_NotaNro = rs_data99!nrofac
        ' Glosa general
        VAR_GLOSA = rs_data99!glosa
        VAR_EstadoId = 11 'Libro Mayor requiere que sean de EstadoId = 10 Cerrado OR EstadoId = 11 Abierto
        VAR_TipoAsientoId = 0 ' Operativo
        VAR_CentroCostoId = rs_data99!CentroCostoId
        VAR_TipoRetencionId = 0
        VAR_TipoId = 0
        VAR_CompDetIdOrg = 0
        VAR_AuxAna = rs_data99!edif_codigo
        ' query_stored
        query_stored = "EXECUTE [dbo].[conta_ingresos] '" & VAR_CODTIPO & "', '" & VAR_PARTIDA & "', " & VAR_EMPRESA & ", " & VAR_DEPTO & ", " & VAR_TIPOCOMPID & ", '" & VAR_FECHA & "', " & VAR_MONEDAID & ", '" & ADec(VAR_TIPOCAMBIO) & "', '" & ADec(VAR_DEBEORG) & "', '" & ADec(VAR_HABERORG) & "', '" & VAR_EntregadoA & "', '" & VAR_CONCEPTO & "', '" & ADec(VAR_PorIVA) & "', '" & ADec(VAR_PorIT) & "', '" & ADec(VAR_PorITF) & "', " & VAR_ConFac & ", " & VAR_SinFac & ", " & VAR_Automatico & ", '" & VAR_GLOSA & "', " & VAR_TipoNotaId & ", " & VAR_NotaNro & ", " & VAR_EstadoId & ", '" & glusuario & "', " & VAR_TipoAsientoId & ", " & VAR_CentroCostoId & ", " & VAR_TipoRetencionId & ", " & VAR_TipoId & ", " & VAR_CompDetIdOrg & ", '" & VAR_AuxAna & "'"
        Debug.Print query_stored
        ' EXEC stored procedured
        Call ExecProcedure(query_stored)
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
Handler:
    If Err.Number <> 0 Then
        MsgBox ("Facturacion " & Err.Number & " : " & Err.Description)
    End If
End Sub

Public Sub Contabiliza_Cobranzas(ByVal IdTraspasoBancos As Long)
    On Error GoTo Handler
    'Contabilizacion al momento de aprobacion
    'Vista relativa a contabilizacion
    Dim rs_data99 As New ADODB.Recordset
    'Declaracion de variables
    Dim VAR_CUENTA As String
    Dim VAR_PARTIDA As String
    Dim VAR_EMPRESA As Integer
    Dim VAR_DPTO As Integer
    Dim VAR_TIPOCOMPID As Integer
    Dim VAR_FECHA As Date
    Dim VAR_TIPOCAMBIO As Double
    Dim VAR_BOB As Double
    Dim VAR_USD As Double
    'Glosas superiores
    Dim VAR_EntregadoA As String
    Dim VAR_CONCEPTO As String
    'Glosa general
    Dim VAR_GLOSA As String
    'Impuestos
    Dim VAR_PorIVA As Double
    Dim VAR_PorIT As Double
    Dim VAR_PorITF As Double
    'Otros valores
    Dim VAR_ConFac As Integer
    Dim VAR_SinFac As Integer
    Dim VAR_Automatico As Integer
    Dim VAR_TipoNotaId As Integer
    Dim VAR_NotaNro As String
    Dim VAR_EstadoId As Integer
    Dim VAR_iConcurrency_id As Integer
    Dim VAR_TipoAsientoId As Integer
    Dim VAR_CentroCostoId As Integer
    Dim VAR_TipoRetencionId As Integer
    Dim VAR_TipoId As Integer
    Dim VAR_CompDetIdOrg As Integer
    Dim VAR_AuxAna As String
    ' Variables adicionales
    Dim query_data As String
    Dim query_stored As String
    ' Data
    query_data = "SELECT [cta], [par], [vtipo], [depto], [fecha], [bs2], [dol2], [edcorto], [eddesc], [nrofac], [doc_nro], [transdes], [contratoOds], [cods], [mes], [correl], [glosa], [codBancarizacion], [soltipo], [notaNro], [CentroCostoId], [edif_codigo] FROM [dbo].[conta_cobranzas] WHERE [cod3] = " & IdTraspasoBancos
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect(query_data)
    If rs_data99.State = adStateClosed Then Exit Sub
    If rs_data99.RecordCount > 0 Then
        rs_data99.MoveFirst
        Do While rs_data99.EOF
            VAR_CUENTA = rs_data99!cta
            VAR_PARTIDA = rs_data99!par
            VAR_EMPRESA = IIf(rs_data99!vtipo = "G", 2, 1)
            VAR_DPTO = rs_data99!depto
            VAR_TIPOCOMPID = 1
            VAR_FECHA = rs_data99!fecha
            VAR_BOB = rs_data99!bs2
            VAR_USD = rs_data99!dol2
            'Glosas superiores
            VAR_EntregadoA = rs_data99!edcorto & " - EDIFICIO: " & rs_data99!eddesc
            VAR_CONCEPTO = "REGISTRO FACTURA NRO. " & rs_data99!nrofac & " CON RECIBO DE COBRANZA NRO. " & rs_data99!doc_nro
            VAR_CONCEPTO = VAR_CONCEPTO & " " & rs_data99!transdes & " EDIFICIO " & rs_data99!edcorto & " " & rs_data99!eddesc
            VAR_CONCEPTO = VAR_CONCEPTO & " S/G " & rs_data99!contratoOds & " " & rs_data99!cods & " " & rs_data99!mes & " TRASP.TES. " & rs_data99!correl
            ' Impuestos
            VAR_PorIVA = 0.13
            VAR_PorIT = 0.03
            VAR_PorITF = 0.0015
            'Glosa general
            VAR_GLOSA = rs_data99!glosa
            'Otros
            VAR_ConFac = 0 'Con factura
            VAR_SinFac = 1 'Sin factura
            VAR_Automatico = 1 '0 Permite edicion, 1 no permite editar
            VAR_TipoNotaId = rs_data99!soltipo
            VAR_NotaNro = rs_data99!notaNro
            VAR_EstadoId = 11 'Libro Mayor requiere que sean de EstadoId = 10 Cerrado OR EstadoId = 11 Abierto
            VAR_TipoAsientoId = 0 'Operativo
            VAR_CentroCostoId = rs_data99!CentroCostoId
            VAR_TipoRetencionId = 0
            VAR_TipoId = 0
            VAR_CompDetIdOrg = 0
            VAR_AuxAna = rs_data99!edif_codigo
            'Otros
            'query stored
            query_stored = "EXEC fp_contabiliza_cobranzas '" & VAR_CUENTA & "', '" & VAR_PARTIDA & "', " & VAR_EMPRESA & ", " & VAR_DPTO & ", " & VAR_TIPOCOMPID & ", '" & VAR_FECHA & "', '" & VAR_BOB & "', '" & VAR_USD & "', '" & VAR_EntregadoA & "', '" & VAR_CONCEPTO & "', '" & VAR_PorIVA & "', '" & VAR_PorIT & "', '" & VAR_PorITF & "', " & VAR_ConFac & ", " & VAR_SinFac & ", " & VAR_Automatico & ", '" & VAR_GLOSA & "', " & VAR_TipoNotaId & ", " & VAR_NotaNro & ", " & VAR_EstadoId & ", '" & glusuario & "', " & VAR_TipoAsientoId & ", " & VAR_CentroCostoId & ", " & VAR_TipoRetencionId & ", " & VAR_TipoId & ", " & VAR_CompDetIdOrg & ", '" & VAR_AuxAna & "'"
            Debug.Print query_stored
            ' EXEC stored procedured
            Call ExecProcedure(query_stored)
            ' Siguiente
            rs_data99.MoveNext
        Loop
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
Handler:
    If Err.Number <> 0 Then
        MsgBox ("Cobranzas " & Err.Number & " : " & Err.Description)
    End If
End Sub

Public Sub Contabiliza_Almacen_Ingreso(ByVal compra_codigo As Long, ByVal adjudica_codigo As Long)
    On Error GoTo Handler
    'Contabilizacion al momento de aprobacion
    ' Vista relativa a contabilizacion
    Dim rs_data99 As New ADODB.Recordset
    ' Declaracion de variables
    Dim VAR_CODTIPO As String
    Dim VAR_PARTIDA As String
    Dim VAR_EMPRESA As Integer
    Dim VAR_DEPTO As Integer
    Dim VAR_TIPOCOMPID As Integer
    Dim VAR_FECHA As Date
    Dim VAR_MONEDAID As Integer
    Dim VAR_TIPOCAMBIO As Double
    Dim VAR_DEBEORG As Double
    Dim VAR_HABERORG As Double
    Dim VAR_EntregadoA As String
    Dim VAR_PorConcepto As String
    'Impuestos
    Dim VAR_PorIVA As Double
    Dim VAR_PorIT As Double
    Dim VAR_PorITF As Double
    'Otros valores
    Dim VAR_ConFac As Integer
    Dim VAR_SinFac As Integer
    Dim VAR_Automatico As Integer
    Dim VAR_GLOSA As String
    Dim VAR_TipoNotaId As Integer
    Dim VAR_NotaNro As Integer
    Dim VAR_EstadoId As Integer
    Dim VAR_iConcurrency_id As Integer
    Dim VAR_TipoAsientoId As Integer
    Dim VAR_CentroCostoId As Integer
    Dim VAR_TipoRetencionId As Integer
    Dim VAR_TipoId As Integer
    Dim VAR_CompDetIdOrg As Integer
    ' Variables auxiliares
    Dim query_data As String '
    Dim query_stored As String
    ' Datos
    query_data = "SELECT [par], [vtipo], [dpto], [fecha], [tm], [tc], [BOB], [des], [denom], [nit], [solic], [esfac], [nrofac], [glosa], [sol_tipo], [nro], [talm], [almtipo] FROM [dbo].[conta_alm_ingreso_eq] WHERE [cod1] = " & compra_codigo & " AND [cod2] = " & adjudica_codigo
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect(query_data)
    If rs_data99.State = adStateClosed Then Exit Sub
    If rs_data99.RecordCount > 0 Then
        rs_data99.MoveFirst
        VAR_CODTIPO = "DEV"
        VAR_PARTIDA = rs_data99!par
        VAR_EMPRESA = IIf(rs_data99!vtipo = "G", 2, 1)
        VAR_DEPTO = rs_data99!dpto
        VAR_TIPOCOMPID = 3 ' Traspaso
        VAR_FECHA = rs_data99!fecha
        VAR_MONEDAID = 1 ' VAR_MONEDAID = IIf(rs_data99!tm = "BOB", 1, 2)
        VAR_TIPOCAMBIO = IIf(IsNull(rs_data99!tc), rs_data99!tc, 6.96)
        VAR_DEBEORG = rs_data99!bob
        VAR_HABERORG = rs_data99!bob
        ' Entregado a
        VAR_EntregadoA = "INGRESO ALMACEN " & IIf(VAR_PARTIDA = 33300, rs_data99!des & " - EPP", rs_data99!des) & " POR SOLICITANTE: " & rs_data99!solic
        ' Por Concepto
        VAR_PorConcepto = "CONTABILIZACION " & IIf(rs_data99!esfac = "SI", "FACTURA", "RECIBO") & " CON NRO. " & rs_data99!nrofac & " DE: " & rs_data99!denom & " - " & rs_data99!nit
        ' Impuestos
        VAR_PorIVA = 0.13
        VAR_PorIT = 0.03
        VAR_PorITF = 0.0015
        ' Otros
        VAR_ConFac = 0
        VAR_SinFac = 1
        VAR_Automatico = 1 'Identifica si es fue generado automaticamente (1) o manual (0)
        VAR_GLOSA = rs_data99!glosa
        VAR_TipoNotaId = rs_data99!sol_tipo
        VAR_NotaNro = rs_data99!nro
        VAR_EstadoId = 11
        VAR_TipoAsientoId = 0
        VAR_CentroCostoId = 0
        VAR_TipoRetencionId = 0
        VAR_TipoId = 0
        VAR_CompDetIdOrg = 0
        ' Procedimiento almacenado
        query_stored = "EXECUTE [dbo].[cont_contabiliza_egresos] '" & VAR_CODTIPO & "', '" & VAR_PARTIDA & "', '', '" & VAR_EMPRESA & "', " & VAR_DEPTO & ", " & VAR_TIPOCOMPID & ", '" & VAR_FECHA & "', " & VAR_MONEDAID & ", '" & ADec(VAR_TIPOCAMBIO) & "', '" & ADec(VAR_DEBEORG) & "', '" & ADec(VAR_HABERORG) & "', '" & VAR_EntregadoA & "', '" & VAR_PorConcepto & "', '" & ADec(VAR_PorIVA) & "', '" & ADec(VAR_PorIT) & "', '" & ADec(VAR_PorITF) & "', " & VAR_ConFac & ", " & VAR_SinFac & ", " & VAR_Automatico & ", '" & VAR_GLOSA & "', " & VAR_TipoNotaId & ", " & VAR_NotaNro & ", " & VAR_EstadoId & ", '" & glusuario & "', " & VAR_TipoAsientoId & ", " & VAR_CentroCostoId & ", " & VAR_TipoRetencionId & ", " & VAR_TipoId & ", " & VAR_CompDetIdOrg & ", @PlanAddCta1 = '', @PlanAddCta2 = ''" '
        Debug.Print query_stored
        ' EXEC stored procedured
        Call ExecProcedure(query_stored)
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
Handler:
    MsgBox ("Almacen de ingreso " & Err.Number & " : " & Err.Description)
End Sub

Public Sub Contabiliza_Almacen_Salida(ByVal venta_codigo As Integer)
    On Error GoTo Handler
    'Contabilizacion al momento de aprobacion
    ' Vista relativa a contabilizacion
    Dim rs_data99 As New ADODB.Recordset
    ' Declaracion de variables
    Dim VAR_CODTIPO As String
    Dim VAR_PARTIDA As String
    Dim VAR_EMPRESA As Integer
    Dim VAR_DEPARTAMENTO As Integer
    Dim VAR_TIPOCOMPID As Integer
    Dim VAR_FECHA As Date
    Dim VAR_MONEDAID As Integer
    Dim VAR_TIPOCAMBIO As Double
    Dim VAR_EntregadoA As String
    Dim VAR_CONCEPTO As String
    Dim VAR_DEBEORG As Double
    Dim VAR_HABERORG As Double
    'Impuestos
    Dim VAR_PorIVA As Double
    Dim VAR_PorIT As Double
    Dim VAR_PorITF As Double
    'Otros valores
    Dim VAR_ConFac As Integer
    Dim VAR_SinFac As Integer
    Dim VAR_Automatico As Integer
    Dim VAR_GLOSA As String
    Dim VAR_TipoNotaId As Integer
    Dim VAR_NotaNro As Integer
    Dim VAR_EstadoId As Integer
    Dim VAR_iConcurrency_id As Integer
    Dim VAR_TipoAsientoId As Integer
    Dim VAR_CentroCostoId As Integer
    Dim VAR_TipoRetencionId As Integer
    Dim VAR_TipoId As Integer
    Dim VAR_CompDetIdOrg As Integer
    ' Variables adicionales
    Dim almacen_tipo As String
    ' query
    Dim query_data As String
    Dim query_stored As String
    ' Consulta SQL - Vista almacenada en MSSQL
    query_data = ""
    'query_data = "SELECT [par_eq], [vtipo], [dpto], [fecha], [tm], [BOB], [USD], [edestino], [ecodigo], [origen], [destino], [glosa], [tiponota], [nro] FROM [dbo].[cont_salida_alm] WHERE [nro] = " & venta_codigo & " AND [unidad] = '" & VAR_UORIGEN & "'"
    If rs_data99.State = adStateOpen Then rs_data99.Close
    Set rs_data99 = GetSelect(query_data)
    If rs_data99.State = adStateClosed Then Exit Sub
    If rs_data99.RecordCount > 0 Then
        ' Determinacion de valores
'        Select Case VAR_UORIGEN
'            Case "UALMI"
'                VAR_CODTIPO = "DII"
'                almacen_tipo = "Insumos"
'            Case "UALMR"
'                VAR_CODTIPO = "DIR"
'                almacen_tipo = "Respuestos"
'            Case "UALMH"
'                VAR_CODTIPO = "DIH"
'                almacen_tipo = "Herramientas"
'            Case Else
'                Exit Sub
'        End Select
        ' Partida
        VAR_PARTIDA = rs_data99!par
        ' EmpresaId
        VAR_EMPRESA = IIf(rs_data99!venta_tipo = "G", 2, 1)
        ' Departamento
        VAR_DEPARTAMENTO = IIf(IsNull(rs_data99!dpto), 0, rs_data99!dpto)
        ' Tipo de comprobante (Trapaso)
        VAR_TIPOCOMPID = 3
        ' Fecha
        VAR_FECHA = rs_data99!fecha
        ' MonedaId
'        If rs_data99!tipo_moneda = "BOB" Then
'            'BOB
'            VAR_MONEDAID = 1
'            VAR_DEBEORG = rs_data99!BS
'            VAR_DEBEORG = rs_data99!BS
'        Else
'            'USD
'            VAR_MONEDAID = 2
'            VAR_DEBEORG = rs_data99!dol2
'            VAR_DEBEORG = rs_data99!dol2
'        End If
        'VAR_MONEDAID = IIf(rs_data99!tipo_moneda = "BOB", 1, 2)
        VAR_MONEDAID = 1
        ' Tipo de cambio
        VAR_TIPOCAMBIO = Round(rs_data99!tipo_cambio, 2)
        ' Cantidad en Bs
        VAR_DEBEORG = rs_data99!bs
        VAR_HABERORG = rs_data99!bs
        ' EntregadoA
        VAR_EntregadoA = "Ingreso a almacenes " & almacen_tipo & " por solicitante: " & rs_data99!solic
        ' PorConcepto
        VAR_CONCEPTO = "Segun factura de: " & rs_data99!denom & " - NIT:" & rs_data99!nit & " con Nro. " & rs_data99!nrofac
        ' Impuestos
        VAR_PorIVA = 0.13
        VAR_PorIT = 0.03
        VAR_PorITF = 0.0015
        ' Otros
        VAR_ConFac = 0
        VAR_SinFac = 0
        VAR_Automatico = 1
        VAR_GLOSA = rs_data99!glosa
        VAR_TipoNotaId = rs_data99!sol_tipo
        VAR_NotaNro = rs_data99!nro
        VAR_EstadoId = 11
        VAR_TipoAsientoId = 0
        VAR_TipoId = 0
        VAR_CompDetIdOrg = 0
        ' Asignacion de query
        query_stored = "EXECUTE [dbo].[cont_contabiliza_egresos] '" & VAR_CODTIPO & "', '" & VAR_PARTIDA & "', '" & 0 & "', " & VAR_EMPRESA & ", " & VAR_DEPARTAMENTO & ", " & VAR_TIPOCOMPID & ", '" & VAR_FECHA & "', " & VAR_MONEDAID & ", '" & VAR_TIPOCAMBIO & "', '" & VAR_DEBEORG & "', '" & VAR_HABERORG & "', '" & VAR_EntregadoA & "', '" & VAR_CONCEPTO & "', '" & VAR_PorIVA & "', '" & VAR_PorIT & "', '" & VAR_PorITF & "', '" & VAR_ConFac & "', '" & VAR_SinFac & "', " & VAR_Automatico & ", '" & VAR_GLOSA & "', " & VAR_TipoNotaId & ", " & VAR_NotaNro & ", " & VAR_EstadoId & ", '" & glusuario & "', " & VAR_TipoAsientoId & ", " & VAR_CentroCostoId & ", " & VAR_TipoRetencionId & ", " & VAR_TipoId & ", " & VAR_CompDetIdOrg & ", '" & 1234 & "', '" & 1234 & "'"
        Debug.Print query_stored
        'MsgBox query_data
        ' EXEC stored procedured
        Call ExecProcedure(query_stored)
    End If
    If rs_data99.State = adStateOpen Then rs_data99.Close
Handler:
    MsgBox ("Almacen de salida " & Err.Number & " : " & Err.Description)
End Sub


