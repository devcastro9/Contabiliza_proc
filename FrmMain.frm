VERSION 5.00
Begin VB.Form FrmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Contabilidad - Soporte"
   ClientHeight    =   8940
   ClientLeft      =   105
   ClientTop       =   435
   ClientWidth     =   18255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   18255
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Caption         =   "Anulacion y Generacion (Identificacion automatica)"
      Height          =   2775
      Left            =   13440
      TabIndex        =   20
      Top             =   360
      Width           =   3975
      Begin VB.CommandButton btnCorreccion 
         Appearance      =   0  'Flat
         BackColor       =   &H00000080&
         Caption         =   "Contabiliza"
         Height          =   495
         Left            =   1080
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1800
         Width           =   1935
      End
      Begin VB.TextBox txtID 
         Height          =   495
         Left            =   960
         TabIndex        =   21
         Top             =   960
         Width           =   2175
      End
      Begin VB.Label Label1 
         Caption         =   "ID Comprobante"
         Height          =   255
         Left            =   480
         TabIndex        =   22
         Top             =   480
         Width           =   2295
      End
   End
   Begin VB.Frame FrameEgreso 
      Caption         =   "Egreso"
      Height          =   1455
      Left            =   10080
      TabIndex        =   15
      Top             =   3600
      Width           =   5535
      Begin VB.OptionButton op5 
         Caption         =   "Comex"
         Height          =   375
         Left            =   3840
         TabIndex        =   8
         Top             =   840
         Width           =   1095
      End
      Begin VB.OptionButton op4 
         Caption         =   "Planilla (RRHH)"
         Height          =   375
         Left            =   3840
         TabIndex        =   7
         Top             =   360
         Width           =   1575
      End
      Begin VB.OptionButton op3 
         Caption         =   "Fondos a rendir"
         Height          =   375
         Left            =   2040
         TabIndex        =   6
         Top             =   360
         Width           =   1575
      End
      Begin VB.OptionButton op2 
         Caption         =   "Salida Almacen"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   840
         Width           =   1575
      End
      Begin VB.OptionButton op1 
         Caption         =   "Ingreso Almacen"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame FrameIngreso 
      Caption         =   "Ingreso"
      Height          =   855
      Left            =   10200
      TabIndex        =   19
      Top             =   3600
      Width           =   5055
      Begin VB.OptionButton opt3 
         Caption         =   "Cobranzas"
         Height          =   255
         Left            =   3600
         TabIndex        =   9
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton opt2 
         Caption         =   "Facturacion"
         Height          =   255
         Left            =   1920
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton opt1 
         Caption         =   "Contratos"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame frmContabilidad 
      Caption         =   "Contabilidad"
      Height          =   975
      Left            =   6960
      TabIndex        =   18
      Top             =   3600
      Width           =   3015
      Begin VB.OptionButton optEgreso 
         Caption         =   "Egreso"
         Height          =   375
         Left            =   1680
         TabIndex        =   1
         Top             =   360
         Width           =   975
      End
      Begin VB.OptionButton optIngreso 
         Caption         =   "Ingreso"
         Height          =   375
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.CommandButton btn 
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      Caption         =   "Contabiliza"
      Height          =   495
      Left            =   12960
      MaskColor       =   &H00FFFFFF&
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   7920
      Width           =   2535
   End
   Begin VB.TextBox txt2 
      Height          =   375
      Left            =   3240
      TabIndex        =   11
      Top             =   4200
      Width           =   2895
   End
   Begin VB.TextBox txt1 
      Height          =   375
      Left            =   3240
      TabIndex        =   10
      Top             =   2640
      Width           =   2895
   End
   Begin VB.Label labelAyuda2 
      Caption         =   "LabelAyuda2"
      Height          =   615
      Left            =   480
      TabIndex        =   17
      Top             =   4800
      Width           =   5655
   End
   Begin VB.Label labelDos 
      Caption         =   "LabelCod2"
      Height          =   375
      Left            =   600
      TabIndex        =   14
      Top             =   4200
      Width           =   2295
   End
   Begin VB.Label labelAyuda1 
      Caption         =   "LabelAyuda1"
      Height          =   615
      Left            =   7440
      TabIndex        =   16
      Top             =   6240
      Width           =   5655
   End
   Begin VB.Label labelUno 
      Caption         =   "LabelCod1"
      Height          =   375
      Left            =   6960
      TabIndex        =   12
      Top             =   5400
      Width           =   2295
   End
End
Attribute VB_Name = "FrmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btn_Click()
    On Error GoTo Handler
    Dim cod1 As Long
    Dim cod2 As Long
    cod1 = 0
    cod2 = 0
    cod1 = Val(txt1.Text)
    cod2 = Val(txt2.Text)
    If optIngreso.Value Then
        If opt1.Value Then
            'Contratos
            Call Contabiliza_Contratos(cod1)
            Debug.Print "Contratos"
        ElseIf opt2.Value Then
            'Facturacion
            Debug.Print "Facturacion"
        ElseIf opt3.Value Then
            'Cobranzas
            Debug.Print "Cobranzas"
        Else
            Debug.Print "Ingreso no definido"
        End If
    ElseIf optEgreso.Value Then
        If op1.Value Then
            'Ingreso almacen
            Debug.Print "Ingreso almacen"
        ElseIf op2.Value Then
            'Salida almacen
            Debug.Print "Salida almacen"
        ElseIf op3.Value Then
            'Fondos a rendir
            Debug.Print "Fondos a rendir"
        ElseIf op4.Value Then
            'Planilla(RRHH)
            Debug.Print "Planilla"
        ElseIf op5.Value Then
            'Comex
            Debug.Print "Comex"
        Else
            Debug.Print "Egreso no definido"
        End If
    Else
        Debug.Print "NN"
    End If
Handler:
    If Err.Number <> 0 Then Debug.Print ("Error en btn " & Err.Number & " : " & Err.Description)
End Sub

Private Sub Form_Load()
    Call CargarMain
    Call CargaControl(Me)
    txt1.Text = 0
    txt2.Text = 0
End Sub

Private Sub op1_Click()
    'Caso Ingreso almacenes
    labelUno.Caption = "Nro de Compra"
    labelAyuda1.Caption = "Tabla: ao_compras_cabecera" & vbCrLf & "Columna: compra_codigo"
    labelDos.Caption = "Nro Adjudica"
    labelAyuda2.Caption = "Tabla: ao_compra_adjudica" & vbCrLf & "Columna: adjudica_codigo"
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = True
    txt2.Visible = True
End Sub

Private Sub op2_Click()
    'Caso Salida almacenes
    labelUno.Caption = "Nro de Venta"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera" & vbCrLf & "Columna: compra_codigo"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub op3_Click()
    'Caso Fondos a rendir
    labelUno.Caption = "XX"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera" & vbCrLf & "Columna: compra_codigo"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub op4_Click()
    'Planilla(RRHH)
    labelUno.Caption = "XXX"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera" & vbCrLf & "Columna: compra_codigo"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub op5_Click()
    'Comex
    labelUno.Caption = "XXXX"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera" & vbCrLf & "Columna: compra_codigo"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub opt1_Click()
    'Caso contratos
    labelUno.Caption = "Codigo de Venta"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera " & vbCrLf & "Columna: venta_codigo"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub opt2_Click()
    'Caso Facturacion
    labelUno.Caption = "Codigo de Venta"
    labelAyuda1.Caption = "Tabla: ao_ventas_cabecera" & vbCrLf & "Columna: venta_codigo"
    labelDos.Caption = "Codigo de Cobranza"
    labelAyuda2.Caption = "Tabla: ao_ventas_cobranza" & vbCrLf & "Columna: cobranza_codigo"
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = True
    txt2.Visible = True
End Sub

Private Sub opt3_Click()
    'Caso Facturacion
    labelUno.Caption = "Id. Traspaso"
    labelAyuda1.Caption = "Tabla: fo_traspaso_bancos" & vbCrLf & "Columna: IdTraspasoBancos"
    labelDos.Caption = ""
    labelAyuda2.Caption = ""
    txt1.Enabled = True
    txt1.Visible = True
    txt2.Enabled = False
    txt2.Visible = False
End Sub

Private Sub optEgreso_Click()
    FrameIngreso.Enabled = False
    FrameIngreso.Visible = False
    FrameEgreso.Enabled = True
    FrameEgreso.Visible = True
    Call op1_Click
End Sub

Private Sub optIngreso_Click()
    FrameEgreso.Enabled = False
    FrameEgreso.Visible = False
    FrameIngreso.Enabled = True
    FrameIngreso.Visible = True
    Call opt1_Click
End Sub
